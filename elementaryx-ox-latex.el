(use-package elementaryx-ox-base)

;; Note: there is no (use-package elementaryx-ox-latex-minimal) directive
;; as emacs-elementaryx-ox-latex-minimal is a dummy package just there for
;; providing dependencies

(use-package elementaryx-ox-latex-classes)

(use-package oc-biblatex :defer t)

(use-package ox-latex
  :defer t
  :config
  ;; Default packages
  ;; Note that: amsmath and amssymv are already included by default in
  ;; org-latex-default-packages-alist
  (add-to-list 'org-latex-packages-alist '("" "cleveref"))
  (add-to-list 'org-latex-packages-alist '("" "braket"))
  (add-to-list 'org-latex-packages-alist '("" "amsopn"))
  (add-to-list 'org-latex-packages-alist '("algo2e,ruled,vlined" "algorithm2e"))
  (add-to-list 'org-latex-packages-alist '("" "tikz"))
  (add-to-list 'org-latex-packages-alist '("" "svg"))
  ;; Refine rendering of pdf source code through =minted=
  (add-to-list 'org-latex-packages-alist '("" "minted"))
  (add-to-list 'org-latex-packages-alist '("" "color"))
  ;;; Ensure that the org-mode #+LANGUAGE: applies during the latex export
  ;;; Seems that babel is already loaded somewhere
  ;;; (add-to-list 'org-latex-packages-alist '("AUTO" "babel" nil)) ;; https://one-octet.dev/posts/org-mode-PDF-export-francais.html
  ;; We define two themes, for light and dark background
  (setq elementaryx/org-latex-minted-options-light-background '(("breaklines" "true")
								("breakanywhere" "true")))
  (setq elementaryx/org-latex-minted-options-dark-background  '(("linenos = false") ("mathescape") ("breaklines")
							    ("style = monokai")))
  (setq org-latex-minted-options elementaryx/org-latex-minted-options-light-background)
  (setq org-latex-listings 'minted)
  ;; We also ease the opportunity to remove packages
  (defun elementaryx/remove-from-org-latex-default-packages-alist (package)
    "Removes `package' from the list of packages that are included by default when
exporting from Org to LaTeX, i.e. from `org-latex-default-packages-alist'. This
is useful for controlling the order in which some of the packages are loaded in
order to prevent conflicts or other undesirable behavior."
    (let ((output '()))
      (dolist (pkg org-latex-default-packages-alist)
	(unless (string= package (nth 1 pkg))
          (add-to-list 'output pkg t)))
      (setq org-latex-default-packages-alist output)))
  (defun elementaryx/remove-from-org-latex-packages-alist (package)
    "Removes `package' from the list of packages that are included by default when
exporting from Org to LaTeX, i.e. from `org-latex-packages-alist'. This is
useful for controlling the order in which some of the packages are loaded in
order to prevent conflicts or other undesirable behavior."
    (let ((output '()))
      (dolist (pkg org-latex-packages-alist)
	(unless (string= package (nth 1 pkg))
          (add-to-list 'output pkg t)))
      (setq org-latex-packages-alist output)))
    :custom
  ;; Override the default LaTeX publishing command.
  (org-latex-pdf-process (list "latexmk --shell-escape -f -pdf -%latex -interaction=nonstopmode -output-directory=%o %f"))
  (org-latex-reference-command "\\cref{%s}")
  (org-latex-prefer-user-labels t) ;; Not required for CUSTOM_ID labels
  ;; ;; We rely on minted for code listing export
  ;; (org-latex-listings 'minted)
  ;; (org-latex-minted-options elementaryx/org-latex-minted-options-light-background)
  )

(use-package ox-koma-letter
  :after org)

(provide 'elementaryx-ox-latex)
